﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.AccessControl;
using System.IO;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using System.Runtime.InteropServices;
using System.Management;
using System.ServiceProcess;
using System.Net.NetworkInformation;
using System.Xml.Linq;
using System.Drawing;


namespace LMS
{
    /// <summary>
    /// Class to search AD for printers or users
    /// </summary>
    public class ADSearcher
    {
        /// <summary>
        /// Events and properties
        /// </summary>

        //public delegate void PrinterAddedHandler(ADPrinter printer);
        //public event PrinterAddedHandler PrinterAdded;
        private DirectorySearcher ds;
        private string searchstring = "";
        private bool isOnline ;

        /// <summary>
        /// Pings the DC to see if its online
        /// </summary>
        /// <returns>True if the PC/DC is connected to the office network</returns>
        public Boolean IsOnline()
        {
            if (!isOnline)
            {
                string logonServer = Environment.ExpandEnvironmentVariables("%logonserver%");
                logonServer = logonServer.Replace("\\", "");
                Ping ping = new Ping();
                try
                {
                    PingReply reply = ping.Send(logonServer, 1000);
                    isOnline = reply.Status == IPStatus.Success;
                    return isOnline;
                }
                catch { return false; }
            }
            return isOnline;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="searchstring">A string representing a search</param>

        public ADSearcher(string searchstring)
        {
            string logonServer = Environment.ExpandEnvironmentVariables("%logonserver%");
            logonServer = logonServer.Replace("\\", "");
            if (IsOnline())
            {
                DirectoryEntry de = new DirectoryEntry("LDAP://" + logonServer);
                ds = new DirectorySearcher(de);
                this.ds.SizeLimit = 0;
                this.ds.CacheResults = true;
                this.searchstring = searchstring;
            }
        }


        /// <summary>
        /// Checks to see if a user is a member of the IT department according to AD
        /// </summary>
        /// <param name="userID">The user's id (firstname.surname format)</param>
        /// <returns></returns>
        public Boolean IsMemberOfIT(string userID)
        {
            if (!IsOnline())
                return false;
            string param = "(&(objectClass=user)(samAccountName=" + userID + "))";
            this.ds.Filter = param;
            SearchResult result = this.ds.FindOne();
            ADUser user = new ADUser(result);
            user.Department = user.Property("department");
            return user.Department.Contains("IT");
        }

        /// <summary>
        /// Gets all the printers found in AD
        /// </summary>
        /// <returns>A list of ADPrinters</returns>
        //public ADPrinters GetPrinters()
        //{
        //    if (!IsOnline())
        //        return new ADPrinters();
        //    //Forms.frmProgress frm = new Forms.frmProgress();
        //    //frm.Show();
        //    ADPrinters printers = new ADPrinters();
        //    string param = "(objectClass=printqueue)";
        //    ds.Filter = param;
        //    SearchResultCollection allPrinters = ds.FindAll();
        //    double total = allPrinters.Count;
        //    double i = 0;

        //    foreach (SearchResult result in allPrinters)
        //    {
        //        i++;
        //        PropertyCollection rs = result.GetDirectoryEntry().Properties;
        //        //frm.SetProgBar("Processing printer " + i + " of " + total, (double) i/total * 100);
        //        ADPrinter printer = new ADPrinter(rs["serverName"].Value.Tostring(),rs["printShareName"].Value.Tostring(), rs["printBinNames"]);
        //        printer.DeviceInfo = rs["driverName"].Value.Tostring();
        //        printers.Add(printer);
        //        this.PrinterAdded(printer);
        //    }
        //    frm.Close();
        //    return printers;
        // }

        /// <summary>
        /// Gets a single user from AD.  The search string is passed to the ADSearcher constructor
        /// </summary>
        /// <returns><An AD user</returns>
        public ADUser GetUser()
        {
            if (!IsOnline())
                return new ADUser(null);
            this.ds.Filter = "(&(objectClass=user)(samAccountName=" + searchstring + "))";

            SearchResult searchResult = ds.FindOne();

            if (searchResult == null)
                return null;

            ADUser user = new ADUser(searchResult);
            user = user.LoadProperties(searchResult);


            return user;
        }
        /// <summary>
        /// Returns multiple users matching criteria supplied to the constructor
        /// </summary>
        /// <returns></returns>
        /// 

            public ADUser GetUserFromEmployeeCode(string employeeID)
        {
            if (!IsOnline())
                return new ADUser(null);
            this.ds.Filter = "(&(employeeID=" + employeeID + "))";

            SearchResult searchResult = ds.FindOne();

            if (searchResult == null)
                return null;

            ADUser user = new ADUser(searchResult);
            user = user.LoadProperties(searchResult);
            return user;
         
        }

        public ADUser GetUserFromCanonincalName()
        {
            if (!IsOnline())
                return new ADUser(null);
            this.ds.Filter = "(&(distinguishedName=" + searchstring + "))";

            SearchResult searchResult = ds.FindOne();

            if (searchResult == null)
                return null;

            ADUser user = new ADUser(searchResult);
            user = user.LoadProperties(searchResult);
            return user;
        }

        public ADUsers GetUsers()
        {
            if (!IsOnline())
                return new ADUsers();
            string[] buffer = searchstring.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            // First iteration for the version 'starts with'
            string param = "(&(objectClass=user){0})";
            string paramConditions = string.Empty;
            if (buffer.Length == 0)
                paramConditions = "(|(givenName=*)(sn=*))";
            else
                if (buffer.Length == 1)
                paramConditions = string.Format("(|(givenName={0}*)(sn={0}*))", buffer[0]);
            else
            {
                paramConditions = string.Format("(&(givenName={0}*)(sn={1}*))", buffer[00], buffer[buffer.Length - 1]);
            }

            ds.Filter = string.Format(param, paramConditions);

            SearchResultCollection searchResults = ds.FindAll();
            ADUsers users = new ADUsers();

            foreach (SearchResult result in searchResults)
            {
                ADUser user = new ADUser(result);
                user = user.LoadProperties(result);
                users.Add(user);
            }


            // Second iteration for the version 'contains'
            paramConditions = string.Empty;

            if (buffer.Length == 1)
                paramConditions = string.Format("(|(givenName=*{0}*)(sn=*{0}*))", buffer[0]);
            else
            {
                for (int index = 0; index < buffer.Length; index++)
                {
                    if (index == 0)
                        paramConditions = "(&(givenName=*" + buffer[index] + "*)";
                    else if (index == buffer.Length - 1)
                        paramConditions = paramConditions + "(sn=*" + buffer[index] + "*))";
                    else
                        paramConditions = paramConditions + string.Format("(|(givenName=*{0}*)(sn=*{0}*))", buffer[index]);
                }
            }

            ds.Filter = string.Format(param, paramConditions);

            searchResults = ds.FindAll();

            if (searchResults == null)
                return users;

            foreach (SearchResult result in searchResults)
            {
                ADUser user = new ADUser(result);

                //if (users.SubSearch(user.Property("sn")).Count == 0)
                if (!users.Contains(user))
                {
                    user = user.LoadProperties(result);
                    users.Add(user);
                }
            }

            return users;
        }

        public ADUsers GetDepartment(string dept)
        {
            if (!IsOnline())
                return new ADUsers();
            string param = $"(&(objectClass=user)(|(givenName=*)(sn=*))(department=*{dept}*))";

            ds.Filter = param;

            SearchResultCollection searchResults = ds.FindAll();
            ADUsers users = new ADUsers();

            foreach (SearchResult result in searchResults)
            {
                ADUser user = new ADUser(result);
                user = user.LoadProperties(result);
                users.Add(user);
            }
            return users;
        }


        /// <summary>
        /// Gets New York users by matching the address with 750*
        /// </summary>
        /// <returns>A list of AD Users</returns>
        public ADUsers GetNYUsers()
        {

            if (!IsOnline())
                return new ADUsers();
            string param = "(&(objectClass=user)(|(givenName=*)(sn=*))(physicalDeliveryOfficeName=750*))";

            ds.Filter = param;

            SearchResultCollection searchResults = ds.FindAll();
            ADUsers users = new ADUsers();

            foreach (SearchResult result in searchResults)
            {
                ADUser user = new ADUser(result);

                users.Add(user);
            }
            return users;
        }

    }



    /// <summary>
    /// A list of ADUsers
    /// </summary>

    public class ADUsers : List<ADUser>
    {
        /// <summary>
        /// Constructors
        /// </summary>
        public ADUsers()
        {

        }
        public ADUsers(IEnumerable<ADUser> users)
            : base(users)
        {

        }
        /// <summary>
        /// Runs a sub search on the list looking for a particular name
        /// </summary>
        /// <param name="searchValue"></param>
        /// <returns>A list of ADUsers</returns>
        public ADUsers SubSearch(string searchValue)
        {
            IEnumerable<ADUser> result =
            (from c in this
             where c.Surname.ToLower().Contains(searchValue.ToLower())
             select c);

            ADUsers users = new ADUsers(result);
            return users;
        }

        /// <summary>
        /// Filters the list for NY users based on their address containing 750
        /// </summary>
        /// <returns>A list of ADUsers</returns>
        public ADUsers NYFilter()
        {
            IEnumerable<ADUser> result =
                (from c in this
                 where c.Office.Contains("750")
                 select c);
            ADUsers users = new ADUsers(result);
            return users;
        }
    }

    /// <summary>
    /// Class handling a user
    /// </summary>
    public class ADUser
    {
        /// <summary>
        /// Properties
        /// </summary>

        private readonly string surname = "";
        private readonly string firstName = "";      
        private readonly string guid = "";
        private readonly string initial = "";
        private readonly string suffix = "";
        private readonly SearchResult searchResult = null;


        public Bitmap Photo { get; set; }
        public Bitmap PhotoFromFolder { get; set; }
        public DateTime StartDate { get; set; }
        public ADUser Manager { get; set; }
        public string Suffix { get; set; }
        public string Initial { get; set; }
        public string Department { get; set; }
        public string Mail { get; set; }
        public string Fax { get; set; }
        public string Title { get; set; }
        public string Login { get; set; }
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string TelephoneNumber { get; set; }
        public string Office { get; set; }
        public string SecurityID { get; set; }
        public string EmployeeID { get; set; }

        public string FullName
        {
            get { return this.firstName + " " + this.surname; }
        }

        public string ExtendedFullName
        {
            get { return (this.firstName + " " + this.initial).Trim() + " " + (this.surname + " " + this.suffix).Trim(); }
        }

        public List<ADGroup> Groups { get; set; }

        /// <summary>
        /// Returns the selected property of the user
        /// </summary>
        /// <param name="property">The property to be returned</param>
        /// <returns>A string representing the property value in AD</returns>
        public string Property(string property)
        {
            try
            {
                if (property.ToLower().Contains("mail"))
                    return this.searchResult.GetDirectoryEntry().Properties[property].Value.ToString().ToLower();
                else
                    return this.searchResult.GetDirectoryEntry().Properties[property].Value.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }

        public DateTime GetDate(SearchResult user)
        {
            return Convert.ToDateTime(user.GetDirectoryEntry().Properties["whenCreated"].Value);
        }
        public Bitmap GetPicture(SearchResult user)
        {

            string path = @"\\Dc1-iaapp01\Employee Pics\" + user.GetDirectoryEntry().Properties["samAccountName"].Value + ".jpg";

            if (File.Exists(path))
            {
                Bitmap b = Bitmap.FromFile(path) as Bitmap;
                return b;
            }

            else
            {


                if (user.GetDirectoryEntry().Properties["thumbnailPhoto"].Value is byte[] data)
                {
                    using (MemoryStream s = new MemoryStream(data))
                    {
                        return Bitmap.FromStream(s) as Bitmap;
                    }
                }
            }
            return null;

        }

        public Bitmap GetPictureFromFolder(ADUser user)
        {
            string path = @"\\Dc1-iaapp01\Employee Pics\";
            Bitmap b = Bitmap.FromFile(path + user.Login + ".jpg") as Bitmap;
            return b;

        }

        public ADUser GetManager(SearchResult user)
        {
            try
            {
                string manager = user.GetDirectoryEntry().Properties["manager"].Value.ToString();
                ADSearcher ads = new ADSearcher(manager);
                return ads.GetUserFromCanonincalName();
            }
            catch { return null; }

        }

        public ADUser LoadProperties(SearchResult searchResult)
        {
            ADUser user = new ADUser(searchResult);
            //user.Surname = user.Property("sn");
            //user.FirstName = user.Property("givenName");
            user.Login = user.Property("samAccountName");
            //user.Title = user.Property("title");
            //user.TelephoneNumber = user.Property("telephoneNumber");
            //user.Fax = user.Property("facsimileTelephoneNumber");
            //user.Mail = user.Property("mail");
            user.Department = user.Property("department");
            //user.Initial = user.Property("initials");
            //user.Suffix = user.Property("extensionAttribute5");
            //user.Office = user.Property("physicalDeliveryOfficeName");
            //user.Manager = user.GetManager(searchResult);
            user.Photo = user.GetPicture(searchResult);
            //user.StartDate = user.GetDate(searchResult);
           // user.Groups = user.GetGroups(searchResult);
            //user.SecurityID = user.Property("securityIdentifier");
            user.EmployeeID = user.Property("employeeID");
            return user;

        }

        public List<ADGroup> GetGroups(SearchResult searchResult)
        {
            List<ADGroup> adg = new List<ADGroup>();
            object[] groups = searchResult.GetDirectoryEntry().Properties["memberOf"].Value as object[];
            foreach (object group in groups)
            {
                ADGroup a = new ADGroup
                {
                    Name = group.ToString()
                };
                adg.Add(a);
            }

            return adg;
        }

        public bool IsMemberOf(string groupName)
        {
            int r = Groups.Count(x => x.Name.ToLower().Contains(groupName.ToLower()));
            return r > 0;
           
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="searchResult">The result returned by ADSearcher</param>
        public ADUser(SearchResult searchResult)
        {
            this.searchResult = searchResult;
            if (searchResult != null)
                this.guid = searchResult.GetDirectoryEntry().Guid.ToString();
        }

        public ADUser() { }


        /// <summary>
        /// Returns the user's name and their login
        /// </summary>
        /// <returns>A string with the user's name and login</returns>
        public override string ToString()
        {
            return this.firstName + " " + this.surname + " [" + this.Login + "]";
        }
        /// <summary>
        /// Compares the two objects and says if they are equal or not
        /// </summary>
        /// <param name="obj">The other object to compare</param>
        /// <returns>True if the two objects are the same</returns>
        public override bool Equals(Object obj)
        {
            if (obj.GetType().Equals(this.GetType()))
            {
                ADUser otherUser = (ADUser)obj;
                return (this.Equals(otherUser));
            }
            else
                return false;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        /// <summary>
        /// Compares the GUIDs of the two users
        /// </summary>
        /// <param name="otherUser">An ADUser object to compare</param>
        /// <returns>True if the two GUIDs are the same</returns>
        public bool Equals(ADUser otherUser)
        {
            return (this.guid == otherUser.guid);
        }
    }

    public class ADGroup
    {
        public string Name { get; set; }
    }

    /// <summary>
    /// Class handling printers returned from AD.  Inherits the Printer object
    /// </summary>
    //public class ADPrinter : Printer
    //{
    //    /// <summary>
    //    /// Properties
    //    /// </summary>
    //    private string serverName;
    //    private string printerName;
    //    private Boolean isNotInXMLFile;

    //    public Boolean IsNotInXMLFile
    //    {
    //        get { return isNotInXMLFile; }
    //        set { isNotInXMLFile = value; }
    //    }

    //    /// <summary>
    //    /// Constructor
    //    /// </summary>
    //    /// <param name="servername">The printer's server</param>
    //    /// <param name="name">The name of the printer</param>
    //    /// <param name="printBinNames">The bin names as returned from AD</param>
    //    public ADPrinter(string servername, string name, PropertyValueCollection printBinNames)
    //    {

    //        this.serverName = servername;
    //        this.printerName = name;
    //        this.Name = string.Format(@"\\{0}\{1}", servername, name);
    //        this.PrintTrays = this.GetTraysFromXML(name);
    //        if (this.PrintTrays.Count == 0)
    //        {
    //            this.isNotInXMLFile = true;
    //            foreach (var p in printBinNames)
    //            {
    //                PrintTray tray = new PrintTray();
    //                tray.TrayName = p.Tostring();
    //                this.PrintTrays.Add(tray);
    //                tray.Printer = this;
    //            }
    //        }
    //        else
    //            this.isNotInXMLFile = false;

    //    }
    //    /// <summary>
    //    /// Constructor
    //    /// </summary>
    //    public ADPrinter()
    //    {

    //    }
    //    /// <summary>
    //    /// Constructor
    //    /// </summary>
    //    /// <param name="name">The name of the printer</param>
    //    public ADPrinter(string name)
    //    {
    //        this.Name = name;
    //    }

    //    /// <summary>
    //    /// Disconnects the printer from the PC
    //    /// </summary>
    //    //public void RemovePrinter()
    //    //{
    //    //    ManagementScope scope = new ManagementScope(@"\root\cimv2");
    //    //    scope.Connect();
    //    //    SelectQuery query = new SelectQuery("select * from Win32_Printer WHERE Name = '" + this.Name.Replace(@"\", @"\\") + "'");
    //    //    ManagementObjectSearcher search = new ManagementObjectSearcher(scope, query);
    //    //    ManagementObjectCollection printers = search.Get();
    //    //    foreach (ManagementObject printer in printers)
    //    //    {
    //    //        printer.Delete();
    //    //        break;
    //    //    }

    //    //}
    //}
    ///// <summary>
    ///// A list of ADPrinters
    ///// </summary>
    //public class ADPrinters : List<ADPrinter>
    //{
    //    public ADPrinters()
    //    {

    //    }
    //}
}
