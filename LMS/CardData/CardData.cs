﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using System.IO;
using System.Drawing;
using LMS.CardValidation;

namespace LMS.CardData
{
    public class CardData
    {
        private const string c_DataSource = @"\\fileserver\Apps\LMS\export.csv";
        public List<EmployeeData> EmployeeData { get; set; }
        public bool Connected { get; set; }

        public CardData()
        {
            try
            {
                if (!File.Exists(c_DataSource))
                {
                    Connected = false;
                    return;
                }

                var csv = new CsvReader(File.OpenText(c_DataSource));
                csv.Configuration.HasHeaderRecord = true;

                EmployeeClassMap cm = new EmployeeClassMap();
                csv.Configuration.RegisterClassMap(cm);

                EmployeeData = csv.GetRecords<EmployeeData>().ToList();
                csv.Dispose();
                AddTranslatedCardNumbers();
                Connected = true;
            }
            catch { Connected = false; }
        }

        public void AddTranslatedCardNumbers()
        {
            foreach (EmployeeData ed in EmployeeData)
                ed.TranslatedCardNumber = ReadingCard.ConvertNumber(ed.CSNDecimal);
        }

       
    }

    public sealed class EmployeeClassMap : ClassMap<EmployeeData>
    {
        public EmployeeClassMap()
        {
            Map(m => m.FirstName).Index(0);
            Map(m => m.LastName).Index(1);
            Map(m => m.Description).Index(2);
            Map(m => m.CardNumber).Index(3);
            Map(m => m.CSNDecimal).Index(4);
            Map(m => m.EmployeeNumber).Index(5);
        }
    }



    public class EmployeeData
    {
        public string FirstName { get; set; } 
        public string LastName { get; set; }
        public string Description { get; set; }
        public string CardNumber { get; set; }
        public string CSNDecimal { get; set; }
        public string EmployeeNumber { get; set; }
        public string UserId { get; set; }
        public string Department { get; set; }
        public Bitmap Photo { get; set; }

        public string TranslatedCardNumber { get; set; }


    }

   

}
