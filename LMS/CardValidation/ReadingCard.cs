﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace LMS.CardValidation
{
    public class ReadingCard
    {
        private System.Timers.Timer _timer = new System.Timers.Timer();
        // private TimeSpan _stoptime;
        private readonly System.Diagnostics.Stopwatch _stopwatch = new System.Diagnostics.Stopwatch();
        public string Data { get; set; }
        public delegate void CardSwiped(string cardId, Form sendingform);
        public event CardSwiped CardIDReceived;
        public delegate void CardSwipeComplete(string cardId, Form sendingform);
        public event CardSwipeComplete CardSwipeCompleted;
        public Form Sender { get; set; }
        public bool CardRead { get; set; }


        public ReadingCard()
        {
         //   _stopwatch.Start();
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Dispose();
            CardRead = true;
            CardIDReceived(Data, Sender);
       }

        public void CardReadCompleted(string cardID)
        {
            CardSwipeCompleted(cardID, Sender);
        }

        public void CardReceived(string cardID)
        {
            CardIDReceived(cardID, Sender);
        }

        public void SendData(string data, Form sender, TimeSpan time)
        {

            if (data != Data)
                CardRead = false;
            Data = data;



            Sender = sender;
            
            if (Data.Length > 0)
                try
                {
           //         _stoptime = _stopwatch.Elapsed;
                    if (Convert.ToUInt64(Data).ToString("X").Length == 8 )
                    {
                        
                        CardRead = true;
                        CardIDReceived(Data, Sender);
                    }
                    else if(Convert.ToUInt64(Data).ToString("X").Length > 8)
                    {
                        CardRead = false;
                        CardIDReceived(Data, Sender);
                    }
                }
                catch
                {
                    CardRead = true;
                    CardIDReceived(Data, Sender);
                }
       }

        public static string ConvertNumber(string cardNumber)
        {
            try
            {
                string hexValue = Convert.ToUInt64(cardNumber).ToString("X");
                string newPairs = hexValue.Substring(6, 2) + hexValue.Substring(4, 2) + hexValue.Substring(2, 2) + hexValue.Substring(0, 2);
                uint output = uint.Parse(newPairs, System.Globalization.NumberStyles.HexNumber);
                return output.ToString();
            }
            catch { return cardNumber; }

            
        }
    }
}
