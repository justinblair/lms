﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace LMS.CardValidation
{
    public class Validation
    {
        private string CardNumber { get; set; }
        public bool Validated { get; set; }

        public Validation (string cardnumber)
        {
            CardNumber = cardnumber;
        }

        public bool Validate()
        {
            return Validated;
        }

    }
}
