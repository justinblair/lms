﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMS
{
    public partial class EventSetter : Form
    {
        private TagReader _tgr;

        public EventSetter()
        {
            InitializeComponent();
        }

        private void EventSetter_Load(object sender, EventArgs e)
        {
            LoadHours(cboHourStart);
            LoadHours(cboHourEnd);
            LoadMinutes(cboMinutesStart);
            LoadMinutes(cboMinuteEnd);
            calStartDate.SetDate(DateTime.Now);
        }

        private void btnLoadCardReader_Click(object sender, EventArgs e)

        {
            DateTime startDate = calStartDate.SelectionStart;
            DateTime endDate = calStartDate.SelectionEnd;
            int.TryParse(cboHourStart.Text, out int starthour);
            int.TryParse(cboHourEnd.Text, out int endhour);
            int.TryParse(cboMinutesStart.Text, out int startminute);
            int.TryParse(cboMinuteEnd.Text, out int endminute);


            CardData.CourseDetails course = new CardData.CourseDetails
            {
                CourseCode = tbxCourseCode.Text,
                LessonCode = tbxLessonCode.Text,
                EventCode = tbxEventCode.Text,
                StartDateTime = new DateTime(startDate.Year, startDate.Month, startDate.Day, starthour, startminute, 0),
                EndDateTime = new DateTime(endDate.Year, endDate.Month, endDate.Day, endhour, endminute, 0),
                Mandatory = chkMandatory.Checked,
                OutputPath = tbxOutput.Text, 
                Location = tbxLocation.Text
            };

            lblWorking.Text = "Working...";
            this.Refresh();
            _tgr = new TagReader(course, this);
            _tgr.Show();
        }

        private void LoadHours(ComboBox cbo)
        {
            for (int i = 6; i < 23; i++)
                cbo.Items.Add(i);
        }

        private void LoadMinutes(ComboBox cbo)
        {
            cbo.Items.Add("00");
            cbo.Items.Add("15");
            cbo.Items.Add("30");
            cbo.Items.Add("45");
        }

        private void picFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.ShowDialog();
            tbxOutput.Text = folderBrowserDialog.SelectedPath;
        }
    }
}
