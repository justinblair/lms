﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMS.CardData;
using LMS.CardValidation;

namespace LMS.Forms
{
    public partial class ManualSignIn : Form
    {

        List<EmployeeData> EmployeeData { get; set; }

        public string CardId { get; set; }

        public ManualSignIn(List<EmployeeData> employeeDatas)
        {
            EmployeeData = employeeDatas;
            InitializeComponent();
            LoadNameList();
        }

        private void LoadNameList()
        {
            List<EmployeeData> filteredList = EmployeeData.Where(x => x.EmployeeNumber != String.Empty).ToList();
            foreach(EmployeeData ed in filteredList)
            {
                var item = cboNameList.Items.Add(ed.FirstName + " " + ed.LastName + ": " + ed.CardNumber);
            }

            cboNameList.Sorted = true;
        }

        private void cboNameList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string cardNumber = cboNameList.Text.Split(':')[1].Trim();
        }

        private void btnSignIn_Click(object sender, EventArgs e)
        {
            CardId = lblCardNumber.Text;
            this.Hide();
        }
    }
}
