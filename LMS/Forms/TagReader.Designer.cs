﻿namespace LMS
{
    partial class TagReader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagReader));
            this.tbxCardNumber = new System.Windows.Forms.TextBox();
            this.lblDataReceived = new System.Windows.Forms.Label();
            this.employeePhoto = new System.Windows.Forms.PictureBox();
            this.chkIncludePhoto = new System.Windows.Forms.CheckBox();
            this.lblSuccess = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.canConnect = new System.Windows.Forms.Label();
            this.canConnectEmployeeData = new System.Windows.Forms.Label();
            this.lblCourseDetails = new System.Windows.Forms.Label();
            this.lblWait = new System.Windows.Forms.Label();
            this.lblLastCard = new System.Windows.Forms.Label();
            this.lblTapCount = new System.Windows.Forms.Label();
            this.lblRegCount = new System.Windows.Forms.Label();
            this.lbxRegistrations = new System.Windows.Forms.ListBox();
            this.chkManualSignIn = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboTimerInterval = new System.Windows.Forms.ComboBox();
            this.lblTest = new System.Windows.Forms.Label();
            this.chkTestMode = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.employeePhoto)).BeginInit();
            this.SuspendLayout();
            // 
            // tbxCardNumber
            // 
            this.tbxCardNumber.BackColor = System.Drawing.Color.Black;
            this.tbxCardNumber.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbxCardNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.tbxCardNumber.Location = new System.Drawing.Point(29, 110);
            this.tbxCardNumber.Margin = new System.Windows.Forms.Padding(4);
            this.tbxCardNumber.Name = "tbxCardNumber";
            this.tbxCardNumber.Size = new System.Drawing.Size(420, 15);
            this.tbxCardNumber.TabIndex = 0;
            this.tbxCardNumber.TextChanged += new System.EventHandler(this.tbxCardNumber_TextChanged);
            this.tbxCardNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbxCardNumber_KeyDown);
            this.tbxCardNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxCardNumber_KeyPress);
            this.tbxCardNumber.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbxCardNumber_KeyUp);
            // 
            // lblDataReceived
            // 
            this.lblDataReceived.AutoSize = true;
            this.lblDataReceived.Font = new System.Drawing.Font("Gill Sans MT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataReceived.ForeColor = System.Drawing.Color.White;
            this.lblDataReceived.Location = new System.Drawing.Point(493, 117);
            this.lblDataReceived.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDataReceived.Name = "lblDataReceived";
            this.lblDataReceived.Size = new System.Drawing.Size(0, 29);
            this.lblDataReceived.TabIndex = 1;
            // 
            // employeePhoto
            // 
            this.employeePhoto.Location = new System.Drawing.Point(496, 202);
            this.employeePhoto.Margin = new System.Windows.Forms.Padding(4);
            this.employeePhoto.Name = "employeePhoto";
            this.employeePhoto.Size = new System.Drawing.Size(245, 223);
            this.employeePhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.employeePhoto.TabIndex = 2;
            this.employeePhoto.TabStop = false;
            // 
            // chkIncludePhoto
            // 
            this.chkIncludePhoto.AutoSize = true;
            this.chkIncludePhoto.Checked = true;
            this.chkIncludePhoto.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIncludePhoto.ForeColor = System.Drawing.Color.White;
            this.chkIncludePhoto.Location = new System.Drawing.Point(31, 136);
            this.chkIncludePhoto.Margin = new System.Windows.Forms.Padding(4);
            this.chkIncludePhoto.Name = "chkIncludePhoto";
            this.chkIncludePhoto.Size = new System.Drawing.Size(115, 21);
            this.chkIncludePhoto.TabIndex = 3;
            this.chkIncludePhoto.Text = "Include photo";
            this.chkIncludePhoto.UseVisualStyleBackColor = true;
            this.chkIncludePhoto.CheckedChanged += new System.EventHandler(this.chkIncludePhoto_CheckedChanged);
            // 
            // lblSuccess
            // 
            this.lblSuccess.AutoSize = true;
            this.lblSuccess.Font = new System.Drawing.Font("Gill Sans MT", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSuccess.ForeColor = System.Drawing.Color.White;
            this.lblSuccess.Location = new System.Drawing.Point(493, 438);
            this.lblSuccess.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSuccess.Name = "lblSuccess";
            this.lblSuccess.Size = new System.Drawing.Size(0, 27);
            this.lblSuccess.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Gill Sans MT", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(24, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 38);
            this.label1.TabIndex = 5;
            this.label1.Text = "Registrations";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Gill Sans MT", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(25, 60);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(304, 27);
            this.label2.TabIndex = 6;
            this.label2.Text = "Please swipe your card on the reader.";
            // 
            // canConnect
            // 
            this.canConnect.AutoSize = true;
            this.canConnect.Font = new System.Drawing.Font("Gill Sans MT", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.canConnect.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.canConnect.Location = new System.Drawing.Point(32, 416);
            this.canConnect.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.canConnect.Name = "canConnect";
            this.canConnect.Size = new System.Drawing.Size(0, 21);
            this.canConnect.TabIndex = 7;
            // 
            // canConnectEmployeeData
            // 
            this.canConnectEmployeeData.AutoSize = true;
            this.canConnectEmployeeData.Font = new System.Drawing.Font("Gill Sans MT", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.canConnectEmployeeData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.canConnectEmployeeData.Location = new System.Drawing.Point(32, 440);
            this.canConnectEmployeeData.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.canConnectEmployeeData.Name = "canConnectEmployeeData";
            this.canConnectEmployeeData.Size = new System.Drawing.Size(0, 21);
            this.canConnectEmployeeData.TabIndex = 8;
            // 
            // lblCourseDetails
            // 
            this.lblCourseDetails.AutoSize = true;
            this.lblCourseDetails.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lblCourseDetails.Location = new System.Drawing.Point(35, 206);
            this.lblCourseDetails.Name = "lblCourseDetails";
            this.lblCourseDetails.Size = new System.Drawing.Size(0, 17);
            this.lblCourseDetails.TabIndex = 9;
            // 
            // lblWait
            // 
            this.lblWait.AutoSize = true;
            this.lblWait.Font = new System.Drawing.Font("Gill Sans MT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWait.Location = new System.Drawing.Point(493, 36);
            this.lblWait.Name = "lblWait";
            this.lblWait.Size = new System.Drawing.Size(60, 29);
            this.lblWait.TabIndex = 10;
            this.lblWait.Text = "label3";
            // 
            // lblLastCard
            // 
            this.lblLastCard.AutoSize = true;
            this.lblLastCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastCard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblLastCard.Location = new System.Drawing.Point(849, 117);
            this.lblLastCard.Name = "lblLastCard";
            this.lblLastCard.Size = new System.Drawing.Size(0, 18);
            this.lblLastCard.TabIndex = 11;
            // 
            // lblTapCount
            // 
            this.lblTapCount.AutoSize = true;
            this.lblTapCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTapCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblTapCount.Location = new System.Drawing.Point(849, 42);
            this.lblTapCount.Name = "lblTapCount";
            this.lblTapCount.Size = new System.Drawing.Size(0, 18);
            this.lblTapCount.TabIndex = 12;
            // 
            // lblRegCount
            // 
            this.lblRegCount.AutoSize = true;
            this.lblRegCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblRegCount.Location = new System.Drawing.Point(849, 69);
            this.lblRegCount.Name = "lblRegCount";
            this.lblRegCount.Size = new System.Drawing.Size(0, 18);
            this.lblRegCount.TabIndex = 13;
            // 
            // lbxRegistrations
            // 
            this.lbxRegistrations.BackColor = System.Drawing.Color.Black;
            this.lbxRegistrations.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbxRegistrations.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxRegistrations.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbxRegistrations.FormattingEnabled = true;
            this.lbxRegistrations.ItemHeight = 18;
            this.lbxRegistrations.Location = new System.Drawing.Point(852, 110);
            this.lbxRegistrations.Name = "lbxRegistrations";
            this.lbxRegistrations.Size = new System.Drawing.Size(309, 360);
            this.lbxRegistrations.TabIndex = 14;
            this.lbxRegistrations.SelectedIndexChanged += new System.EventHandler(this.lbxRegistrations_SelectedIndexChanged);
            // 
            // chkManualSignIn
            // 
            this.chkManualSignIn.AutoSize = true;
            this.chkManualSignIn.ForeColor = System.Drawing.Color.White;
            this.chkManualSignIn.Location = new System.Drawing.Point(164, 135);
            this.chkManualSignIn.Margin = new System.Windows.Forms.Padding(4);
            this.chkManualSignIn.Name = "chkManualSignIn";
            this.chkManualSignIn.Size = new System.Drawing.Size(151, 21);
            this.chkManualSignIn.TabIndex = 15;
            this.chkManualSignIn.Text = "Use manual sign-in";
            this.chkManualSignIn.UseVisualStyleBackColor = true;
            this.chkManualSignIn.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(32, 482);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 17);
            this.label3.TabIndex = 17;
            this.label3.Text = "Reset time";
            // 
            // cboTimerInterval
            // 
            this.cboTimerInterval.BackColor = System.Drawing.Color.Black;
            this.cboTimerInterval.ForeColor = System.Drawing.Color.White;
            this.cboTimerInterval.FormattingEnabled = true;
            this.cboTimerInterval.Location = new System.Drawing.Point(115, 479);
            this.cboTimerInterval.Name = "cboTimerInterval";
            this.cboTimerInterval.Size = new System.Drawing.Size(96, 24);
            this.cboTimerInterval.TabIndex = 18;
            this.cboTimerInterval.SelectedIndexChanged += new System.EventHandler(this.cboTimerInterval_SelectedIndexChanged);
            // 
            // lblTest
            // 
            this.lblTest.AutoSize = true;
            this.lblTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTest.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblTest.Location = new System.Drawing.Point(232, 481);
            this.lblTest.Name = "lblTest";
            this.lblTest.Size = new System.Drawing.Size(46, 20);
            this.lblTest.TabIndex = 19;
            this.lblTest.Text = "Test";
            this.lblTest.Click += new System.EventHandler(this.lblTest_Click);
            // 
            // chkTestMode
            // 
            this.chkTestMode.AutoSize = true;
            this.chkTestMode.ForeColor = System.Drawing.Color.White;
            this.chkTestMode.Location = new System.Drawing.Point(293, 483);
            this.chkTestMode.Margin = new System.Windows.Forms.Padding(4);
            this.chkTestMode.Name = "chkTestMode";
            this.chkTestMode.Size = new System.Drawing.Size(97, 21);
            this.chkTestMode.TabIndex = 20;
            this.chkTestMode.Text = "Test mode";
            this.chkTestMode.UseVisualStyleBackColor = true;
            this.chkTestMode.CheckedChanged += new System.EventHandler(this.chkTestMode_CheckedChanged);
            // 
            // TagReader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1191, 507);
            this.Controls.Add(this.chkTestMode);
            this.Controls.Add(this.lblTest);
            this.Controls.Add(this.cboTimerInterval);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.chkManualSignIn);
            this.Controls.Add(this.lbxRegistrations);
            this.Controls.Add(this.lblRegCount);
            this.Controls.Add(this.lblTapCount);
            this.Controls.Add(this.lblLastCard);
            this.Controls.Add(this.lblWait);
            this.Controls.Add(this.lblCourseDetails);
            this.Controls.Add(this.canConnectEmployeeData);
            this.Controls.Add(this.canConnect);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblSuccess);
            this.Controls.Add(this.chkIncludePhoto);
            this.Controls.Add(this.employeePhoto);
            this.Controls.Add(this.lblDataReceived);
            this.Controls.Add(this.tbxCardNumber);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "TagReader";
            this.Text = "Please register...";
            this.Load += new System.EventHandler(this.TagReader_Load);
            ((System.ComponentModel.ISupportInitialize)(this.employeePhoto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxCardNumber;
        private System.Windows.Forms.Label lblDataReceived;
        private System.Windows.Forms.PictureBox employeePhoto;
        private System.Windows.Forms.CheckBox chkIncludePhoto;
        private System.Windows.Forms.Label lblSuccess;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label canConnect;
        private System.Windows.Forms.Label canConnectEmployeeData;
        private System.Windows.Forms.Label lblCourseDetails;
        private System.Windows.Forms.Label lblWait;
        private System.Windows.Forms.Label lblLastCard;
        private System.Windows.Forms.Label lblTapCount;
        private System.Windows.Forms.Label lblRegCount;
        private System.Windows.Forms.ListBox lbxRegistrations;
        private System.Windows.Forms.CheckBox chkManualSignIn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboTimerInterval;
        private System.Windows.Forms.Label lblTest;
        private System.Windows.Forms.CheckBox chkTestMode;
    }
}