﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMS.CardValidation;
using LMS.CardData;
using CsvHelper;
using System.IO;
using System.Diagnostics;

namespace LMS
{
    public partial class TagReader : Form
    {

       public LMSIntegration.HTTPAPI HTTP { get; set; }
       private const string c_OutputPath = @"\\fileserver\Apps\LMS\Registrations";
       private Stopwatch _stopwatch = new Stopwatch(); 
      private ReadingCard _readingCard;
       private readonly List<EmployeeData> _employeeData;
       private ADSearcher _adSearcher;
       private ADUser _currentUser;
       private EmployeeData _currentEmployeeData;
       private string _cardID;
       private string _convertedCardNumber = string.Empty;
       private bool _success;
       private readonly CardData.CourseDetails _courseDetails;
       private List<CardData.CSVData> _csvData;
       private int _registrationCount = 0;
       private List<EmployeeData> _employeeRegistrations = new List<EmployeeData>();
       private Forms.ManualSignIn _manualSignIn;
       private System.Timers.Timer _timer;
       private const int c_defaultTimeOut = 500;
        private static Random _random = new Random();
        private bool _testMode = false;
       

        public TagReader(CardData.CourseDetails courseDetails, EventSetter sender)
        {
            InitializeComponent();
            // LMSIntegration.HTTPAPI m = new LMSIntegration.HTTPAPI();
            // m.Connect();
            _stopwatch = new Stopwatch();
            _csvData = new List<CSVData>();
            _courseDetails = courseDetails;
            _readingCard = new ReadingCard();
            _readingCard.CardIDReceived += _readingCard_CardIDReceived;
            _readingCard.CardSwipeCompleted += _readingCard_CardSwipeCompleted;
            CardData.CardData cd = new CardData.CardData();
            _adSearcher = new ADSearcher(String.Empty);
            SetTimerCombo();
           // lblTest.Visible = false;

            ADUser user = _adSearcher.GetUserFromEmployeeCode("4228");
            _employeeData = cd.EmployeeData;
            _adSearcher.GetUserFromEmployeeCode("4869");
            if (ActivityRecording.CanConnect())
                canConnect.Text = "Connection to database established.";
            else
                canConnect.Text = "Unable to connect to database.";
            if (cd.Connected)
                canConnectEmployeeData.Text = "Access to card record data established";
            else
                canConnectEmployeeData.Text = "Unable to establish connected to card record data";

            LoadCourseDetails();
            sender.Controls["lblWorking"].Text = "Ready";
       }

        private void SetTimerCombo()
        {
            for (int i = 500; i < 5000; i += 100)
                cboTimerInterval.Items.Add(i);

            cboTimerInterval.Text = c_defaultTimeOut.ToString();

        }

        private void SetTimer()
        {
            if(_timer!=null)
            try
            {
                _timer.Interval = int.Parse(cboTimerInterval.Text);
            }
            catch { _timer.Interval = c_defaultTimeOut; }
        }

        private void Wait(int millisecs)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            while (stopwatch.ElapsedMilliseconds < millisecs) { }
            return;
        }

        private void TestReader()
        {
            chkTestMode.Checked = true;
            List<EmployeeData> employeesOnly = _employeeData.Where(x => x.EmployeeNumber != string.Empty).ToList();

            int r = _random.Next(employeesOnly.Count);
            EmployeeData ed = employeesOnly[r];
            string cardNumber = ed.TranslatedCardNumber;
            // string cardNumber = ReadingCard.ConvertNumber(ed.CSNDecimal);
            //string cardHex2 = Int64.Parse(newCardNber).ToString("X");
            Keyboard kb= new Keyboard();
            for (int i = 0; i < cardNumber.Length; i++)
            {
                string chr = cardNumber.Substring(i, 1);
                kb.Send("KEY_" + chr);
                Wait(10);
               
                tbxCardNumber_KeyDown(null, null);
          }

            //tbxCardNumber.Text = string.Empty;
            this.Refresh();
            //_testMode = false;
          
        }

        

        private void StartTimer()
        {
            if (_timer != null)
               if (_timer.Enabled)
                    _timer.Dispose();
           
            _timer = new System.Timers.Timer();
            SetTimer();
            _timer.Elapsed += _timer_Elapsed;
            _timer.Start();
        }

        private void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            string cardID = tbxCardNumber.Text;
            _timer.Stop();
            _timer.Dispose();

            if (cardID.Length > 0)
            {
                this.Invoke((MethodInvoker)delegate { this.ClearTextBox(); });
                this.Invoke((MethodInvoker)delegate { AnalyseData(cardID); });
                this.Invoke((MethodInvoker)delegate { this.UpdateControls(); });
                this.Invoke((MethodInvoker)delegate { this.UpdateTapCount(); });
                if (!_testMode)
                {
                    _registrationCount++;
                    this.Invoke((MethodInvoker)delegate { this.PleaseTryAgain(); });
                    _currentEmployeeData = new EmployeeData() { FirstName = "Not", LastName = "Found" };
                    this.Invoke((MethodInvoker)delegate { this.UpdateList(cardID); });
                    CreateDummyRegistrationAndLog(cardID);
                }
            }
       }

        private void CreateDummyRegistrationAndLog(string cardID)
        {
            Registrations reg = CreateDummyRegistration(cardID);
            ActivityRecording.RecordActivity(reg);
        }

        private List<EmployeeData> CacheADData(List<EmployeeData> data)
        {
            
            foreach (var item in data)
            {
                if (item.EmployeeNumber != string.Empty)
                {
                    ADUser user = _adSearcher.GetUserFromEmployeeCode(item.EmployeeNumber);
                    if (user != null)
                    {
                        item.UserId = user.Login;
                        item.Department = user.Department;
                        item.Photo = user.PhotoFromFolder;
                    }
                    else
                    {
                        item.UserId = "Employee " + item.EmployeeNumber + " not in AD.";
                    }
                }
            }
            return data;
        }

        private void LoadCourseDetails()
        {
            lblCourseDetails.Text = 
                "Course code: " + _courseDetails.CourseCode + Environment.NewLine +
                "Lesson code: " + _courseDetails.LessonCode + Environment.NewLine +
                "Event code " + _courseDetails.EventCode + Environment.NewLine +
                "Start time: " + _courseDetails.StartDateTime + Environment.NewLine +
                "End time: " + _courseDetails.EndDateTime + Environment.NewLine +
                "Mandatory: " + (_courseDetails.Mandatory ? "Yes" : "No");
        }

        private void _readingCard_CardSwipeCompleted(string cardId, Form sendingform)
        {
            sendingform.Invoke((MethodInvoker)delegate { this.UpdateList(cardId); });
            _employeeRegistrations.Add(_currentEmployeeData);
            sendingform.Invoke((MethodInvoker)delegate { this.UpdateRegCount(); });
        }

        

        private void _readingCard_CardIDReceived(string cardId, Form Sender)
        {
            Sender.Invoke((MethodInvoker)delegate { this.ClearTextBox();});
            Validation validation = new Validation(cardId);
            _stopwatch.Stop();
            Sender.Invoke((MethodInvoker) delegate { this.UpdateControls(cardId);});
            _readingCard.CardRead = true;
            _registrationCount++;
            Sender.Invoke((MethodInvoker)delegate { this.UpdateTapCount(); });
       }

        private void UpdateRegCount()
        {
            lblRegCount.Text = "Number of registrations: " + _employeeRegistrations.Count;
        }

        private void UpdateTapCount()
        {
            lblTapCount.Text = "Number of taps: " + _registrationCount;
        }

        private void UpdateList(string cardId)
        {
            lbxRegistrations.Items.Insert(0, cardId + "   " + _currentEmployeeData.FirstName + " " + _currentEmployeeData.LastName);
            lbxRegistrations.SelectedItem = 0;
            lbxRegistrations.Update();
        }

        private void ClearTextBox()
        {
            tbxCardNumber.Text = String.Empty;
            this.Refresh();
        }

        private void CreateCSV()
        {
            LMS.CardData.CSVData csv = new CSVData
            {   UserName = _currentUser.Login,
                CourseCode = _courseDetails.CourseCode,
                LessonCode = _courseDetails.LessonCode,
                EventCode = _courseDetails.EventCode,
                Mandatory = _courseDetails.Mandatory ? "Yes" : "No",
                Status = "Attended",
                StartDateTime = _courseDetails.StartDateTime,
                EndDateTime = _courseDetails.EndDateTime,
                Location = _courseDetails.Location
            };

            _csvData.Add(csv);

            string filePath;

            if (_courseDetails.OutputPath.Length == 0)
                filePath = Path.Combine(c_OutputPath, "Registrations." + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + "." + ".csv");
            else
                filePath = Path.Combine(_courseDetails.OutputPath, _courseDetails.CourseCode + ".csv");

            bool requiresHeader = !File.Exists(filePath);

            using (var writer = new CsvWriter(new StreamWriter(filePath,true)))
            {
                if (requiresHeader)
                    writer.WriteHeader(csv.GetType());
                writer.NextRecord();
                writer.WriteRecord(csv);
            }

        }

        private void PleaseTryAgain()
        {
            lblSuccess.Text = "Please try again.";
            lblSuccess.ForeColor = Color.LightGreen;
        }

        private void SetSuccessMessage()
        {
            if (_success)
            {
                lblSuccess.Text = "Registration successful.";
                lblSuccess.ForeColor = Color.LightGreen;
            }
            else
            {
                lblSuccess.Text = "Registration failed.";
                lblSuccess.ForeColor = Color.Red;
            }
        }

        private void AddFailToListBox(string cardID)
        {
            lbxRegistrations.Items.Add(cardID + " not found");
        }

        private void UpdateControls()
        {
            lblWait.Text = "Ready.";
            lblWait.ForeColor = Color.LightGreen;
            this.Refresh();
        }

        private async void UpdateControls(string cardId)
        {
            UpdateControls();
           await DoWork(cardId);
        }

        private Task<string> DoWork(string cardId)
        {
           string myCard = ReadingCard.ConvertNumber("2194293539");
           tbxCardNumber.TextChanged -= tbxCardNumber_TextChanged;

            EmployeeData ed = LookupEmployee(cardId, out bool found);
            if (!found)
                LoadManualSignin();

            ADUser user = null;

            if (ed.EmployeeNumber == String.Empty)
            {
                lblDataReceived.Text = "Not found..." + Environment.NewLine +
                    "Temporary card?";
                ed.FirstName = "Temporary";
                ed.LastName = "Card?";
                _currentEmployeeData = ed;
                _currentUser = new ADUser();
                _currentUser.Login = "temp.card?";
                _cardID = cardId;
                tbxCardNumber.Enabled = true;
                tbxCardNumber.Focus();
                LoadManualSignin();
           }
            else
            {
                try
                {
                    user = _adSearcher.GetUserFromEmployeeCode(ed.EmployeeNumber);
                }
                catch
                {
                    user = new ADUser();
                }
                _currentEmployeeData = ed;
                _currentUser = user;
                _cardID = cardId;
            }

            try
            {

                lblDataReceived.Text = ed.FirstName + " " + ed.LastName + Environment.NewLine +
                    _currentUser.Department;
                if (chkIncludePhoto.Checked)
                    this.employeePhoto.Image = _currentUser.Photo;
            }
            catch { }

                  
            tbxCardNumber.Focus();
            try
            {
                CompleteRegistration(cardId, this);
            }
            catch
            { CreateDummyRegistrationAndLog(cardId); }

            _readingCard.CardReadCompleted(cardId);
            return new Task<string>(ToString);

        }
 
        private void LoadManualSignin()
        {

            if(chkManualSignIn.Checked)
            if (_manualSignIn == null)
            {
                _manualSignIn = new Forms.ManualSignIn(_employeeData);
                    _manualSignIn.Deactivate += _manualSignIn_Deactivate;
                    _manualSignIn.Show();
            }
            else
                _manualSignIn.Show();

        }

        private void _manualSignIn_Deactivate(object sender, EventArgs e)
        {
            tbxCardNumber.Text = _manualSignIn.CardId;
        }

        private void TagReader_Load(object sender, EventArgs e)
        {
            tbxCardNumber.Focus();
        }

        private EmployeeData LookupEmployee(string cardNumber, out bool found)
        {
           // string convcardNumber = ReadingCard.ConvertNumber(cardNumber).Trim();
           //_convertedCardNumber = convcardNumber;
            try
            {
                found = true;
                return _employeeData.Where(x => x.TranslatedCardNumber == cardNumber).First();
            }
            catch
            {
                found = false;
                return new EmployeeData() { FirstName = "Not", LastName = "Found" };
            }
         
        }


        private Registrations CreateDummyRegistration(string cardID)
        {
            Registrations reg = new Registrations()
            {
                FirstName = "Not",
                LastName = "Found",
                DateTime = DateTime.Now,
                CardNumber = cardID,
                EmployeeID ="0000",
                Department = "???",
                Email = "not.found",
                DeviceID = Environment.MachineName
            };
            return reg;
        }

        private Registrations CreateRegistration()
        {
            Registrations reg = new Registrations()
            {
                FirstName = _currentEmployeeData.FirstName,
                LastName = _currentEmployeeData.LastName,
                DateTime = DateTime.Now,
                CardNumber = _convertedCardNumber,
                EmployeeID = _currentEmployeeData.EmployeeNumber,
                Department = _currentUser.Department,
                Email = _currentUser.Login,
                DeviceID = Environment.MachineName
            };
            return reg;
        }

        private void CompleteRegistration(string cardId, Form sendingform)
        {
            try
            {
                Registrations reg = CreateRegistration();
                _success = ActivityRecording.RecordActivity(reg);
                CreateCSV();
                sendingform.Invoke((MethodInvoker)delegate { this.SetSuccessMessage(); });
            }
            catch { CreateDummyRegistrationAndLog(cardId); }
        }

        private void chkIncludePhoto_CheckedChanged(object sender, EventArgs e)
        {
            tbxCardNumber.Focus();
        }

        private void tbxCardNumber_KeyPress(object sender, KeyPressEventArgs e)

        { 
        }

        private void tbxCardNumber_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void tbxCardNumber_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                AnalyseData(tbxCardNumber.Text);
            }
            catch { }
        }

        private void tbxCardNumber_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
               AnalyseData(tbxCardNumber.Text);
            }
            catch { }


        }

        private void AnalyseData(string CardID)
        {
            if(!_stopwatch.IsRunning)
                _stopwatch.Start();
            int length = 0;
            try {
                if(CardID.Length>0)
                    length = Convert.ToUInt64(CardID).ToString("X").Length; }
            catch
            {
               // tbxCardNumber.Text = string.Empty;
                return;
            }

            if (length < 8 && length > 0)
            {
                //if(!_testMode)
                     StartTimer();
                return;
            }



            if (length == 8)
            {
                EmployeeData ed = LookupEmployee(CardID, out bool found);

                if (found)
                {
                    if (_stopwatch.IsRunning)
                    {
                        try
                        {
                            _timer.Dispose();
                        }
                        catch { }

                        string cardID = CardID;
                       
                        _readingCard.SendData(cardID, this, _stopwatch.Elapsed);
                    }
                    _stopwatch.Restart();
                    tbxCardNumber.Text = string.Empty;
                    this.Refresh();
                }
                else return;


            }
            else if (length > 8)
                tbxCardNumber.Text = string.Empty;
        }

        private void lbxRegistrations_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbxCardNumber.Focus();
        }

        private void cboTimerInterval_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetTimer();
            tbxCardNumber.Focus();
        }

        private void lblTest_Click(object sender, EventArgs e)
        {
            TestReader();
        }

        private void chkTestMode_CheckedChanged(object sender, EventArgs e)
        {
            _testMode = chkTestMode.Checked;
            tbxCardNumber.Focus();
        }
    }

      

}
