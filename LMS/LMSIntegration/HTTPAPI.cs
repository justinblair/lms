﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using LMS.CardValidation;
using System.Net.Http.Headers;
using System.Windows.Forms;
using Newtonsoft.Json;


namespace LMS.LMSIntegration
{
    public class HTTPAPI
    {
        private const string c_URI = @"https://mishcon.kallidus-suite.com/api/v1/";
        private const string c_DOMAIN = "https://mishcon.kallidus-suite.com/";
        private const string c_Token = @"f5efadbc-2b5e-4570-96da-fc78c898cccf]";
        public HttpClient _httpClient { get; set; }
       

        public HTTPAPI()
        {

        }

       

        public string UseWebRequest()
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(c_URI);
            webRequest.Accept = "application/json;odata=verbose";
            webRequest.ContentType = "application/json;odata=verbose";
            webRequest.Headers.Add(HttpRequestHeader.Authorization, @"SVC_MDRKalidus:]K#iN{m;");
           
            webRequest.Method = "POST";


            var webResponse =  (HttpWebResponse)webRequest.GetResponse();
            return webResponse.ToString();
        }

        public void Connect()
        {

            if (_httpClient == null)
                _httpClient = new HttpClient();

            _httpClient.BaseAddress = new Uri(c_DOMAIN); //https://mishcon.kallidus-suite.com/

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(@"SVC_MDRKalidus", @"]K#iN{m;");
            Task<HttpResponseMessage> t = _httpClient.GetAsync("external/");
            HttpResponseMessage h = t.Result; //Result = 200, OK

            t = _httpClient.GetAsync("kip/api/authenticate/");
            h = t.Result; //Result = 401, Unauthorised

            t = _httpClient.GetAsync("api/v1/users/");
            h = t.Result; //Result = 401, Unauthorised

        }


        public async Task<HttpResponseMessage> GetAsync(string request)
        {
            HttpResponseMessage response = await _httpClient.GetAsync(request);
            response.EnsureSuccessStatusCode();
            return response;

        }




        // HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, "kip/api/authenticate/");
        // //httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue(@"SVC_MDRKalidus", @"]K#iN{m;");
        // var response = _httpClient.SendAsync(httpRequestMessage);
        // var s = response.Result; //Result = 200. OK

        //Task<HttpResponseMessage> v = _httpClient.GetAsync("api/v1/users/");
        //HttpResponseMessage hq = v.Result; //Result = 401, Unauthorised





        //  try
        //  {
        //      response = _httpClient.SendAsync(httpRequestMessage2);
        //  }
        //  catch { }

        //  s = response.Result;



        //  httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, "api/v1/users");
        //  httpRequestMessage.Headers.Add("WWW-Authenticate", @"Saml realm = ""https://mishcon.kallidus-suite.com/api/v1/"" domain = ""https://mishcon.kallidus-suite.com/kip/api/authenticate""");

        //  httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue(@"SVC_MDRKalidus", @"]K#iN{m;");
        //  //httpRequestMessage.Headers.Add("Authorization", Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(@"SVC_MDRKalidus:]K#iN{m;")));


        //s = response.Result;





    }

       



    }

