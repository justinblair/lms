﻿namespace LMS
{
    partial class SignIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxCardNumber = new System.Windows.Forms.TextBox();
            this.tbxNotice = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tbxCardNumber
            // 
            this.tbxCardNumber.BackColor = System.Drawing.SystemColors.Desktop;
            this.tbxCardNumber.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbxCardNumber.ForeColor = System.Drawing.Color.White;
            this.tbxCardNumber.Location = new System.Drawing.Point(28, 32);
            this.tbxCardNumber.Name = "tbxCardNumber";
            this.tbxCardNumber.Size = new System.Drawing.Size(178, 13);
            this.tbxCardNumber.TabIndex = 0;
            this.tbxCardNumber.TextChanged += new System.EventHandler(this.TbxCardNumber_TextChanged);
            // 
            // tbxNotice
            // 
            this.tbxNotice.BackColor = System.Drawing.SystemColors.Desktop;
            this.tbxNotice.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbxNotice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.tbxNotice.Location = new System.Drawing.Point(271, 32);
            this.tbxNotice.Name = "tbxNotice";
            this.tbxNotice.Size = new System.Drawing.Size(178, 13);
            this.tbxNotice.TabIndex = 1;
            // 
            // SignIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tbxNotice);
            this.Controls.Add(this.tbxCardNumber);
            this.Name = "SignIn";
            this.Text = "Sign in...";
            this.Load += new System.EventHandler(this.SignIn_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxCardNumber;
        private System.Windows.Forms.TextBox tbxNotice;
    }
}

