﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMS.LMSIntegration;
using LMS.CardValidation;

namespace LMS
{
    public partial class SignIn : Form

       {
        
        private int EventId { get; set; }

        public SignIn(int eventID)
        {
            InitializeComponent();
            EventId = eventID;
        }

        private void SignIn_Load(object sender, EventArgs e)
        {
            tbxCardNumber.Focus();
        }

        private void TbxCardNumber_TextChanged(object sender, EventArgs e)
        {
            this.tbxCardNumber.TextChanged -= new System.EventHandler(this.TbxCardNumber_TextChanged);
            Validation val = new Validation(tbxCardNumber.Text);

            bool validated = val.Validate();

            if (validated)
                this.tbxCardNumber.Text = String.Empty;
            else
                this.tbxNotice.Text = "Not recognised.";


            this.tbxCardNumber.TextChanged += new System.EventHandler(this.TbxCardNumber_TextChanged);


        }
    }
}
